// random.h
// written by j.friant, 8/9/2000
//
// Just so I could have a couple of random number routines
//

int rollDice(short sides, short number);
float rollDice_f(float sides, short number);
void initRand();
