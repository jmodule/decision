//-------------------//
// random.cpp        //
// j.friant 8/9/2000 //
//-------------------//

#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "random.h"

//---------------------------------------------------------------------
int rollDice(short sides, short number)
{
  int roll;

  roll = 0;
  for (int i = 0; i < number; i++) {
    roll += (int) ceil(drand48() * sides);
  }
  return roll;
}

float rollDice_f(float sides, short number)
{
  float roll;

  roll = 0;
  for (int i = 0; i < number; i++) {
    roll += ceil(drand48() * sides);
  }
  return roll;
}

void initRand()
{
  srand48((long int)time((time_t *)NULL));
}
