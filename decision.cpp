// $Id: decision.cpp,v 1.4 2005-11-23 18:41:42 jfriant Exp $
//
// Idea from http://www.io.com/~hampster/
// The Matrix Game by Chris Engle, Copyright 2000 by the same.  This program
// was written to simulate a solo game.  Where the player types in an argument
// and the strength of the argument.  The program rolls the dice and tells if
// if the player was successful.  If the argument failed it will try to negate
// the sentence and asks the player if the sentance should be kept or if they
// want to write a new one.  The sentences are saved in a file - defaulting to
// decision.txt.  The file is opened to append and a date stamp written to it.
//

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string>
#include "random.h"

using namespace std;

using std::string;
//
// function prototypes
//
short getFile(ofstream & outfile, string filename);
int getArgument(string & argument, int & strength);

////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
  string       argument[2];
  int          roll=0;
  short        exit_loop=0, exit_prog=0;
  int          succeed[2] = {0,0};
  int          strength[2] = {0,0};
  unsigned int arg_cnt = 0;
  ofstream     outfile;
  string       filename;

  //
  // First check the command line options
  //
  int i = 1;
  while (i < argc)
    {
      if (strncmp(&argv[i][0], "-", 1) != 0)
        filename = argv[i];
      i++;
    }
  
  initRand(); // initialize our random number generator

  //
  // Intro stuff
  //
  
  cout << endl
       << "========================" << endl
       << "The Basic Matrix Game" << endl
       << "By Chris Engle" << endl << endl
       << "Programming by J.Friant" << endl
       << "========================" << endl << endl;

  if (getFile(outfile, filename) == 0)
    return 1;
  //
  // Main loop
  //

  while (1) {
    for (int which=0; which<2; which++) {
      exit_prog = getArgument(argument[which], strength[which]);
      if (exit_prog == 1)
        return 0;
    } //end for 

    exit_loop = 0;

    for (int which=0, iter=0; which<2; which++) {

      roll = rollDice(6, 1);

      if (roll <= strength[which]) {
        succeed[which] = 1;
        cout << "Argument " << which+1 << " Succeeded";
      }
      else {
        succeed[which] = 0;
        cout << "Argument " << which+1 << " Failed";
      }
      cout << " (roll = " << roll << ")" << endl;
      short test = succeed[0] ^ succeed[1];
      if (test == 0 && which >= 1) {
        which = -1; // reset the loop; they both can't succeed
        iter++;
        if (iter>100)
          break;    // I only want to fail x number of times
      }
    } //end for

    outfile << "==> " << ++arg_cnt << endl;
    for (int which=0; which<2; which++) {
      if (succeed[which])
        outfile << "Succeeded: " << argument[which] << endl;
      else
        outfile << "Failed: " << argument[which] << endl;
    }
  } //end while
  return 0;
}

////////////////////////////////////////////////////////////////////////
short getFile(ofstream & outfile, string filename = "")
{
  time_t curr_time;
  short  fine = 0;

  if (filename == "")
    {
      cout << "Enter the file name for the history file"
           << " (default=decision.txt): ";
      getline((istream &)cin, filename, '\n');

      if ("" == filename)
        filename = "decision.txt";
    }

  cout << "Using: " << filename << endl;

  //
  // Open and initialize the file by adding a date stamp before begining
  //

  time(&curr_time); // fill with the current time and date
 
  outfile.open(filename.c_str(), ios::app);
  if (outfile) {
    fine = 1;
    outfile << endl << "---- " << ctime(&curr_time) << endl;
  }
  else
    cout << "Error opening " << filename << endl;
  
  return fine;
}

////////////////////////////////////////////////////////////////////////
int getArgument(string & argument, int & strength)
{
  short  fine;
  string str;

  do {
    fine = 1;
    cout << "Enter argument (q to quit): ";
    getline((ifstream &)cin, argument, '\n');
    
    if (argument == "q" || argument == "Q") {
      return 1; // exiting program !!
    }
    else
      if (argument == "?")
        {
          fine = 0;
          cout << "Enter an action statement supported by arguments about why it should happen." << endl << endl;
        }
  } while (0 == fine);

  fine = 0;
  do {
    cout << "Enter strength of argument (?=help): ";
    getline((ifstream &)cin, str, '\n');

    if (str == "?") {
      cout << "5 = Very Strong" << endl
           << "4 = Strong" << endl
           << "3 = Average" << endl
           << "2 = Weak" << endl
           << "1 = Very Weak" << endl << endl;
    }
    else {
      strength = atoi(str.c_str());
      if (strength < 1 && strength > 4) {
        cout << "Strength must be between 1 and 5" << endl;
        fine = 0;
      }
      else
        fine = 1;
    }
  } while (fine == 0);
  return 0;
}
