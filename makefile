decision : decision.o random.o
	g++ -o decision decision.o random.o

random.o : random.cpp random.h
	g++ -c random.cpp

decision.o : decision.cpp
	g++ -c decision.cpp

clean:
	rm -f *.o *.txt decision
